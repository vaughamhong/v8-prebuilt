################################################################################

# save off the local path
LOCAL_PATH_TEMP := $(LOCAL_PATH)
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

################################################################################

LOCAL_MODULE := icudata
LOCAL_SRC_FILES := libs/Release/libicudata.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)
include $(PREBUILT_STATIC_LIBRARY)

LOCAL_MODULE := icui18n
LOCAL_SRC_FILES := libs/Release/libicui18n.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)
include $(PREBUILT_STATIC_LIBRARY)

LOCAL_MODULE := icuuc
LOCAL_SRC_FILES := libs/Release/libicuuc.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)
include $(PREBUILT_STATIC_LIBRARY)

LOCAL_MODULE := v8_base
LOCAL_SRC_FILES := libs/Release/libv8_base.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)
include $(PREBUILT_STATIC_LIBRARY)

LOCAL_MODULE := v8_libbase
LOCAL_SRC_FILES := libs/Release/libv8_libbase.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)
include $(PREBUILT_STATIC_LIBRARY)

LOCAL_MODULE := v8_libplatform
LOCAL_SRC_FILES := libs/Release/libv8_libplatform.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)
include $(PREBUILT_STATIC_LIBRARY)

LOCAL_MODULE := v8_nosnapshot
LOCAL_SRC_FILES := libs/Release/libv8_nosnapshot.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)
include $(PREBUILT_STATIC_LIBRARY)

LOCAL_MODULE := v8_snapshot
LOCAL_SRC_FILES := libs/Release/libv8_snapshot.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)
include $(PREBUILT_STATIC_LIBRARY)

################################################################################

# Restore the local path since we overwrote it when we started
LOCAL_PATH := $(LOCAL_PATH_TEMP)

################################################################################