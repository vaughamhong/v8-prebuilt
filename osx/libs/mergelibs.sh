lipo -create Release_x64/libv8_base.a Release_i386/libv8_base.a -o Release/libv8_base.a
lipo -create Release_x64/libv8_libbase.a Release_i386/libv8_libbase.a -o Release/libv8_libbase.a
lipo -create Release_x64/libv8_libplatform.a Release_i386/libv8_libplatform.a -o Release/libv8_libplatform.a
lipo -create Release_x64/libv8_nosnapshot.a Release_i386/libv8_nosnapshot.a -o Release/libv8_nosnapshot.a
lipo -create Release_x64/libv8_snapshot.a Release_i386/libv8_snapshot.a -o Release/libv8_snapshot.a
lipo -create Release_x64/libicudata.a Release_i386/libicudata.a -o Release/libicudata.a
lipo -create Release_x64/libicui18n.a Release_i386/libicui18n.a -o Release/libicui18n.a
lipo -create Release_x64/libicuuc.a Release_i386/libicuuc.a -o Release/libicuuc.a

lipo -create Debug_x64/libv8_base.a Debug_i386/libv8_base.a -o Debug/libv8_base.a
lipo -create Debug_x64/libv8_libbase.a Debug_i386/libv8_libbase.a -o Debug/libv8_libbase.a
lipo -create Debug_x64/libv8_libplatform.a Debug_i386/libv8_libplatform.a -o Debug/libv8_libplatform.a
lipo -create Debug_x64/libv8_nosnapshot.a Debug_i386/libv8_nosnapshot.a -o Debug/libv8_nosnapshot.a
lipo -create Debug_x64/libv8_snapshot.a Debug_i386/libv8_snapshot.a -o Debug/libv8_snapshot.a
lipo -create Debug_x64/libicudata.a Debug_i386/libicudata.a -o Debug/libicudata.a
lipo -create Debug_x64/libicui18n.a Debug_i386/libicui18n.a -o Debug/libicui18n.a
lipo -create Debug_x64/libicuuc.a Debug_i386/libicuuc.a -o Debug/libicuuc.a